/**
 * Created by mizan on 14/07/17.
 */

(function($) {

    $.fn.techmasters = function(options) {


        var settings = $.extend({
            text         : 'techmasters',
            color        : null,
            fontStyle    : null,
            success      : null
        }, options);


        //console.log(settings);


        if($.isFunction(settings.success)) {
            settings.success.call(this);
        }


        //console.log(this)

        this.each(function(){

            //console.log(this)

            $(this).text(settings.text+" "+$(this).text());

            if(settings.color)
                $(this).css("color",settings.color);

        })


    }

}(jQuery));
